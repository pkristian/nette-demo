FROM php:8.1.0-apache

RUN a2enmod rewrite

RUN apt-get update \
&&  docker-php-ext-install mysqli \
&&  docker-php-ext-enable mysqli


COPY --chown=www-data:www-data . /var/www/html
