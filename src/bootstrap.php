<?php declare(strict_types=1);

include_once __DIR__ . '/common.php';
    /** @var \Nette\Bootstrap\Configurator $configurator */
return $configurator->createContainer();
