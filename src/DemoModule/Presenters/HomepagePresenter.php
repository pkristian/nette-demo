<?php declare(strict_types=1);


namespace App\DemoModule\Presenters;


use JetBrains\PhpStorm\NoReturn;

class HomepagePresenter extends BasePresenter
{

    public function renderDefault(): void
    {

    }


    #[NoReturn] public function handleResetBrands(): void
    {
        $this->m->getDbBrand()->reset();
        $this->redirect('this');
    }
}
