<?php declare(strict_types=1);

namespace App\DemoModule\Presenters;

use JetBrains\PhpStorm\NoReturn;
use Nette,
    App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{


    public function startup()
    {
        parent::startup();


        $this->template->title = 'Nette Demo - Patrik Kristian';
        $this->template->headline = '{headline}';
    }


    public function beforeRender()
    {

        parent::beforeRender();
    }


    #[NoReturn] protected function flashMessageRefresh(string $message, string $type): void
    {
        $this->flashMessage($message, $type);
        $this->redirect('this');
    }

}
