<?php declare(strict_types=1);


namespace App\DemoModule\Presenters;


class BrandPresenter extends BasePresenter
{


    /** @persistent */
    public int $page = 1;

    /** @persistent */
    public int $perPage = 10;

    /** @persistent */
    public bool $asc = true;

    public int $count;

    public int $maxPage;


    public function actionDefault(): void
    {
        $this->count = $this->m->getDbBrand()->countAll();

        $this->maxPage = (int)ceil($this->count / $this->perPage);

    }


    public function renderDefault(): void
    {
        $this->template->count = $this->count;
        $this->template->perPage = $this->perPage;
        $this->template->page = $this->page;
        $this->template->maxPage = $this->maxPage;
        $this->template->asc = $this->asc;

        $this->template->tableData = $this->m->getDbBrand()
            ->findForGrid(
                $this->perPage,
                $this->page,
                $this->asc,
            )
        ;
    }


    public function handleDeleteBrand(int $brandId): void
    {
        $this->m->getDbBrand()->deleteById($brandId);

        $this->flashMessageRefresh('Značka byla smazána.', 'blue');

    }


    public function handleUpdateBrand(?int $brandId, string $brandName): void
    {
        $brandNameType = \App\Model\Types\BrandName::fromNull($brandName);
        if (is_null($brandNameType)) {
            $this->flashMessageRefresh('Název je nevalidní', 'red');
        }
        /* bacause phpstan is sometimes dumb */
        /** @var \App\Model\Types\BrandName $brandNameType */

        if ($brandId) {
            $brand = $this->m->getDbBrand()->findById($brandId);
            if (!$brand) {
                $this->flashMessageRefresh('Značnu nelze nalézt.', 'red');
            }
        } else {
            $brand = new \App\Model\Dataset\BrandDataset();
        }
        /** @var \App\Model\Dataset\BrandDataset $brand */

        $brand->setName($brandNameType);

        try {
            $brand->save();
        } catch (\Dibi\UniqueConstraintViolationException $e) {
            $this->flashMessageRefresh('Značka se stejným názvem již existuje.', 'red');
        }

        if ($brandId) {
            $this->flashMessageRefresh('Značka byla přejmenována.', 'blue');
        } else {
            $this->flashMessageRefresh('Značka byla vytvořena.', 'green');
        }
    }
}
