<?php declare(strict_types=1);

namespace App\Core;

use     Nette\Application\Routers\RouteList;


/**
 * Router factory.
 */
class RouterFactory
{

    public static function createRouter(): RouteList
    {
        $router = new RouteList;
        $router
            ->withModule('Demo')
            ->addRoute('brand/[p:<page>][pp:<perPage>][asc:<asc>]', 'Brand:default')
            ->addRoute('[<presenter>/[<action>[/<id>]]]', 'Homepage:default')
        ;

        return $router;
    }

}
