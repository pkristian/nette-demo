<?php declare(strict_types=1);
/* app wide constants */

const DIR_APP = __DIR__;
const DIR_ROOT = DIR_APP . '/../';

const DIR_VAR = DIR_ROOT . '/var';
const DIR_LOG = DIR_VAR . '/log';
const DIR_TEMP = DIR_VAR . '/temp';
const DIR_SESSIONS = DIR_VAR . '/sessions';

const DIR_CONFIG = DIR_ROOT . '/config';
const DIR_TEMPLATES = DIR_ROOT . '/templates';


require DIR_ROOT . 'vendor/autoload.php';

$configurator = new \Nette\Configurator;

$configurator->addParameters(['DIR_ROOT' => DIR_ROOT]);
$configurator->addParameters(['DIR_LOG' => DIR_LOG]);
$configurator->addParameters(['DIR_TEMP' => DIR_TEMP]);

if ((bool)@$_ENV['DEBUG_ON']) {
    $configurator->setDebugMode(true);
}

if (!file_exists(DIR_LOG)) {
    mkdir(DIR_LOG, 0777, true);
}
$configurator->enableDebugger(DIR_LOG);
if (!file_exists(DIR_TEMP)) {
    mkdir(DIR_TEMP, 0777, true);
}
$configurator->setTempDirectory(DIR_TEMP);

$configurator->createRobotLoader()
    ->addDirectory(DIR_APP)
    ->register()
;


$configurator->addConfig(DIR_CONFIG . '/config.neon');

$localConfig = DIR_CONFIG . '/config.local.neon';
if (file_exists($localConfig)) {
    $configurator->addConfig($localConfig);
}

$configurator->addParameters(["DIR_ROOT" => DIR_ROOT]);

