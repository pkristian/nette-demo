<?php declare(strict_types=1);

namespace App\Presenters;

use Nette,
    App\Model,
    Tracy\Debugger;


/**
 * Error presenter.
 */
class ErrorPresenter extends BasePresenter
{

    /**
     * @param \Exception $exception
     *
     * @return void
     * @throws Nette\Application\AbortException
     */
    public function renderDefault(\Exception $exception): void
    {
        if ($exception instanceof Nette\Application\BadRequestException) {
            $code = $exception->getCode();
            // load template 403.latte or 404.latte or ... 4xx.latte
            $this->setView((string)(in_array($code, [403, 404, 405, 410, 500]) ? $code : '4xx'));
            // log to access.log
            Debugger::log(
                "HTTP code $code: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}",
                'access'
            );

        } else {
            $this->setView('500'); // load template 500.latte
            Debugger::log($exception, Debugger::EXCEPTION); // and log exception
            if ($this->context->getParameters()['consoleMode']) {
                echo "\n****************************************";
                echo "\n***   500 server error, go see log   ***";
                echo "\n****************************************";
                echo "\n***   " . $exception->getMessage();
                exit;
            } else {
                throw $exception;
            }
        }

        if ($this->isAjax()) { // AJAX request? Note this error in payload.
            $this->payload->error = true;
            $this->terminate();
        }
    }

}
