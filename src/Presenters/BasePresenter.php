<?php declare(strict_types=1);

namespace App\Presenters;

use Nette,
    App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /**
     * @inject
     * @var \App\Model\ModelFactory
     */
    public \App\Model\ModelFactory $m;


    /**
     * Formats view template file names.
     * @return array<int,string>
     */
    public function formatTemplateFiles(): array
    {
        [$module, $presenter] = \Nette\Application\Helpers::splitName((string)$this->getName());

        $view = ucfirst($this->view);

        return [
            DIR_TEMPLATES . "/$module/$presenter/$view.latte",
        ];

    }


}
