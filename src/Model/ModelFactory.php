<?php declare(strict_types=1);


namespace App\Model;


/**
 * --db
 * @method Db\BrandModel getDbBrand()
 */
final class ModelFactory
{

    private \Nette\DI\Container $container;


    private \Dibi\Connection $connection;

    /**
     * @var array<string,object>
     */
    private array $models = [];

    private static self $instance;


    public function __construct(\Nette\DI\Container $container, \Dibi\Connection $connection)
    {
        if (isset(self::$instance)) {
            throw new \Exception('One ModelFactory is already defined!');
        }
        self::$instance = $this;

        $this->container = $container;
        $this->connection = $connection;
    }


    public static function getInstance(): self
    {
        return self::$instance;
    }


    public function getContainer(): \Nette\DI\Container
    {
        return $this->container;
    }


    public function getConnection(): \Dibi\Connection
    {
        return $this->connection;
    }


    /**
     * @param string $name
     * @param array<string,mixed> $arguments
     *
     * @return mixed
     */
    public function __call(string $name, array $arguments): mixed
    {
        $reflection = new \ReflectionClass($this);
        $doc = $reflection->getDocComment();

        $re = '/@method\s+(?\'className\'[^\s]+)\s+(' . $name . ')\(/m';
        preg_match($re, (string)$doc, $matches);
        $className = $matches['className'];
        if ($className) {
            $modelClass = $reflection->getNamespaceName() . '\\' . $className;

            return $this->returnModel($modelClass);
        }

        trigger_error('Call to undefined method ' . __CLASS__ . '::' . $name . '()', E_USER_ERROR);
    }


    /**
     * @param string $modelClass
     *
     * @return mixed
     */
    private function returnModel(string $modelClass): mixed
    {
        $modelName = $modelClass;

        if (!array_key_exists($modelClass, $this->models)) {

            $this->models[$modelName] = new $modelClass($this);
        }

        return $this->models[$modelName];
    }


    public function save(\App\Model\Dataset\BaseDataset $dataset, array $options = []): mixed
    {
        return $this->returnModel((string)$dataset::MODEL_CLASS)->save($dataset, $options);
    }


    public function blockBegin(): void
    {
        $this->getConnection()->begin();

    }


    public function blockEnd(bool $commit): void
    {
        if ($commit) {
            $this->blockCommit();
        } else {
            $this->blockRollback();
        }

    }


    public function blockCommit(): void
    {
        $this->getConnection()->commit();

    }


    public function blockRollback(): void
    {
        $this->getConnection()->rollback();

    }

}