<?php declare(strict_types=1);


namespace App\Model\Dataset;


use App\Model\Types\BrandName;

class BrandDataset extends BaseDataset
{

    const TABLE = 'brand';
    const VIEW = 'brand';
    const MODEL_CLASS = \App\Model\Db\BrandModel::class;

    public const COL_NAME = 'name';


    protected BrandName $name;


    public static function import(iterable $row): static
    {
        $r = self::innerImport($row);
        $r->setName($row[self::COL_NAME]);

        return $r;
    }


    /**
     * @return array<string, mixed>
     */
    protected function innerExport(): array
    {
        $r = parent::innerExport();
        $r[self::COL_NAME] = $this->getName();

        return $r;
    }


    public function getName(): BrandName
    {
        return $this->name;
    }


    public function setName(string|BrandName $name): static
    {
        $this->name = BrandName::from($name);

        return $this;
    }


}