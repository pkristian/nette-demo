<?php declare(strict_types=1);


namespace App\Model\Dataset;


/**
 * @property-read \App\Model\ModelFactory $m
 */
abstract class BaseDataset implements DatasetInterface
{

    const TABLE = null;
    const VIEW = null;
    const MODEL_CLASS = null;

    const COL_ID = 'id';

    protected int $id;


    public function __construct()
    {

    }


    public function __get(string $name):object
    {
        if ($name === "m") {
            return \App\Model\ModelFactory::getInstance();
        }
        throw new \Exception('Property ' . $name . ' doesn\'t exist');
    }


    final public function export(): array
    {
        $export = $this::innerExport();

        foreach ($export as &$value) {
            if ($value instanceof \App\Model\Types\IType) {
                $value = $value->s();
            }

            if (is_array($value)) {
                $value = json_encode($value);
            }

        }

        return $export;
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }


    public function issetId(): bool
    {
        return isset($this->id);
    }


    public function unsetId(): static
    {
        unset($this->id);

        return $this;
    }


    public static function importMultiple(iterable $rowList, bool $preserveKeys = false): array
    {
        $datasetList = [];
        foreach ($rowList as $index => $row) {
            $dataset = static::import($row);
            if ($preserveKeys) {
                $datasetList[$index] = $dataset;
            } else {
                $datasetList[$dataset->getId()] = $dataset;
            }
        }

        return $datasetList;
    }


    protected static function innerImport(iterable $row): static
    {
        // no floats allowed... make them strings back
        // this should make no harm
        // see: \Dibi\Result::normalize
        foreach ($row as &$item) {
            if (is_float($item)) {
                $item = (string)$item;
            }
        }


        /** @phpstan-ignore-next-line */
        $r = new static;
        $r->setId($row[static::COL_ID]);


        return $r;
    }


    /**
     * @return array<string,mixed>
     */
    protected function innerExport(): array
    {
        $r = [];
        $r['id'] = $this->getId();

        return $r;
    }


    public function save(array $options = []): static
    {
        return $this->m->save($this, $options);
    }


    /* static stuff */

    public function __clone()
    {
        $this->unsetId();
    }


    public static function importNull(?iterable $row): ?static
    {
        if ($row) {
            return static::import($row);
        }

        return null;
    }


    /**
     * @param iterable $row
     */
    abstract public static function import(iterable $row): static;


}
