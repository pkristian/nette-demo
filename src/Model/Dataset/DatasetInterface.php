<?php declare(strict_types=1);


namespace App\Model\Dataset;


interface DatasetInterface
{

    public function getId(): int;

}