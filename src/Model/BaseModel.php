<?php declare(strict_types=1);


namespace App\Model;


abstract class BaseModel
{

    protected ModelFactory $m;


    public function __construct(ModelFactory $m)
    {
        $this->m = $m;
        $this->afterConstruct();
    }


    protected function afterConstruct():void
    {

    }



}