<?php declare(strict_types=1);


namespace App\Model\Db;

use App\Model\Dataset\BaseDataset;

abstract class BaseModel extends \App\Model\BaseModel
{

    const TABLE = '';
    const VIEW = '';
    const DATASET_CLASS = '';

    const SAVE_SETTING_TABLE_NAME = 'tableName';
    const SAVE_SETTING_EXCLUDE_COLUMNS = 'excludeColumns';
    const SAVE_SETTING_ONLY_COLUMNS = 'onlyColumns';
    const SAVE_SETTING_FORCE_INSERT = 'forceInsert';


    protected function afterConstruct(): void
    {
        parent::afterConstruct();
    }


    protected function db(): \Dibi\Connection
    {
        return $this->m->getConnection();
    }


    protected function selectView(string $as = null): \Dibi\Fluent
    {
        $wildcard = '*';

        if ($as) {
            $wildcard = $as . '.' . $wildcard;
        }

        $fluent = $this->db()
            ->select($wildcard)
        ;
        if ($as) {
            return $fluent->from($this::VIEW, 'AS ' . $as);

        };

        return $fluent->from($this::VIEW);
    }


    protected function findBy(mixed ...$where): ?\Dibi\Row
    {
        /** @var ?\Dibi\Row $r */
        $r = $this->selectView()->where(...$where)->fetch();

        return $r;

    }


    /**
     * @param mixed ...$where
     *
     * @return \Dibi\Row[]
     */
    protected function findByAll(...$where): array
    {
        return $this->selectView()->where(...$where)->fetchAll();

    }


    public function save(
        BaseDataset $dataset,
        array $options = []
    ): BaseDataset {

        $presentInDb = true;
        if (!$dataset->issetId()) {
            $presentInDb = false;
            $dataset->setId($this->generateId());
        }


        $table = @$options[self::SAVE_SETTING_TABLE_NAME] ?: static::TABLE;
        $data = $dataset->export();

        $keysExclude = @$options[self::SAVE_SETTING_EXCLUDE_COLUMNS];
        if (is_array($keysExclude)) {
            foreach ($keysExclude as $key) {
                unset($data[$key]);
            }
        }

        $keysOnly = @$options[self::SAVE_SETTING_ONLY_COLUMNS];
        if (is_array($keysOnly)) {
            foreach ($data as $key => $value) {
                if (
                    $key != BaseDataset::COL_ID //always include ID
                    &&
                    !in_array($key, $keysOnly)
                ) {
                    unset($data[$key]);
                }
            }
        }

        $forceInsert = @$options[self::SAVE_SETTING_FORCE_INSERT];
        if ($presentInDb && !$forceInsert) {
            $this->saveUpdate(
                dataset: $dataset,
                data: $data,
                table: $table,
                options: $options,
            );
        } else {
            $this->saveInsert(
                dataset: $dataset,
                data: $data,
                table: $table,
                options: $options,
            );
        }

        return $dataset;
    }


    private function saveUpdate(
        BaseDataset $dataset,
        array $data,
        string $table,
        array $options = []
    ): void {
        $this->db()->update($table, $data)
            ->where("%n = %s", BaseDataset::COL_ID, $dataset->getId())
            ->execute()
        ;
    }


    private function saveInsert(
        BaseDataset $dataset,
        array $data,
        string $table,
        array $options = []
    ): void {
        $query = $this->db()->insert($table, $data)->__toString();
        $result = $this->db()->nativeQuery($query);
        if (!$result->count()) {
            $dataset->unsetId();
        }
    }


    private function generateId(): int
    {
        return (int)($this->selectView()->orderBy(['id' => 'DESC'])->fetchSingle()) + 1;
    }


}
