<?php declare(strict_types=1);


namespace App\Model\Db;


use App\Model\Dataset\BrandDataset as Dataset;

class BrandModel extends \App\Model\Db\BaseModel
{

    const TABLE = Dataset::TABLE;
    const VIEW = Dataset::VIEW;
    const DATASET_CLASS = Dataset::class;


    public function findById(int $id): ?Dataset
    {
        $result = $this->db()
            ->select('*')
            ->from(self::VIEW)
            ->where([Dataset::COL_ID => $id])
            ->fetch()
        ;

        return Dataset::importNull($result);
    }


    /**
     * @return Dataset[]
     */
    public function findAll(): array
    {
        return Dataset::importMultiple(
            $this->selectView()->orderBy([Dataset::COL_NAME => 'ASC'])
        );
    }


    /**
     * @return Dataset[]
     */
    public function findForGrid(
        int $perPage,
        int $page,
        bool $asc
    ): array {
        return Dataset::importMultiple(
            $this->selectView()
                ->orderBy([Dataset::COL_NAME => $asc ? 'ASC' : 'DESC'])
                ->offset($perPage * ($page - 1))
                ->limit($perPage)
        );
    }


    /**
     * @return int
     */
    public function countAll(): int
    {
        return $this->selectView()->count();
    }


    public function deleteById(int $id): void
    {
        $this->db()
            ->delete($this::TABLE)
            ->where([
                Dataset::COL_ID => $id,
            ])
            ->execute()
        ;
    }


    public function reset(): void
    {
        $this->db()->loadFile(__DIR__ . '/reset.sql');

    }

}