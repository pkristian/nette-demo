<?php declare(strict_types=1);


namespace App\Model\Types;

use App\Sorry\SorryTypeValidation as TypeException;

interface IType
{
    /**
     * @return static
     * @throws TypeException
     */
    public static function from(mixed $s): self;


    public static function fromNull(mixed $s): ?self;


    public function __toString(): string;


    public function s(): string;




}
