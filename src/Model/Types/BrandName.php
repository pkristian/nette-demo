<?php declare(strict_types=1);


namespace App\Model\Types;


use App\Sorry\SorryTypeValidation as TypeException;

class BrandName implements IType
{

    use TypeTrait;


    private const REGEX = /** @lang RegExp */
        <<<'EOT'
%
^
(?'printable'[^<>\n]){0}
(
    (?&printable){1,225}
)

$
%x
EOT;


    public function __construct(
        protected string $value,
    ) {
        $valid = preg_match(self::REGEX, $this->value);

        if (!$valid) {
            throw new TypeException();
        }
    }


    protected static function innerFrom(string $s): self
    {
        $s = trim($s);

        if ($s) {
            return new self(
                $s
            );
        }

        throw new TypeException();
    }


    public function __toString(): string
    {
        return $this->value;
    }


}
