<?php declare(strict_types=1);


namespace App\Model\Types;

use App\Sorry\SorryTypeValidation as TypeException;

trait TypeTrait
{

    /**
     * @param string $s
     *
     * @throws TypeException
     */
    abstract protected static function innerFrom(string $s): self;


    /**
     * @throws TypeException
     */
    public static function from(mixed $s): self
    {
        if ($s instanceof self) {
            return $s;
        }

        return static::innerFrom((string)$s);
    }


    final public static function fromNull(mixed $s): ?self
    {
        try {
            return static::from($s);
        } catch (TypeException $e) {
        }

        return null;
    }


    abstract public function __toString(): string;


    public function s(): string
    {
        return $this->__toString();
    }


}
