#!/usr/bin/env bash
set -e

cd "$(dirname "$0")"

docker-compose \
  down

echo -e "\e[33mDeleting var...\e[0m"
rm -rf ../var

echo -e "\e[33mCreating dirs...\e[0m"
mkdir -m a=rwx ./../var
mkdir -m a=rwx ./../var/log
mkdir -m a=rwx ./../var/sessions
mkdir -m a=rwx ./../var/temp

docker-compose \
  up \
  --build \
  --remove-orphans \
  -d

