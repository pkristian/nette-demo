#!/usr/bin/env bash

cd "$(dirname "$0")"


docker-compose exec -T app \
    vendor/bin/phpstan analyse -c phpstan.neon --ansi "$@"


